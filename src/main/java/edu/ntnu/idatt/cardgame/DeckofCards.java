package edu.ntnu.idatt.cardgame;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

public class DeckofCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private final int[] face = IntStream.rangeClosed(1, 13).toArray();
    public ArrayList<PlayingCard> deck;

    public DeckofCards() {
        deck = new ArrayList<>();
        for (char s: suit) {
            for (int f : face){
                PlayingCard card = new PlayingCard(s, f);
                deck.add(card);
            }
        }
    }

    public ArrayList<PlayingCard> dealHand(int n){
        if(n < 53){
            Random random = new Random();
            ArrayList<PlayingCard> deal = new ArrayList<>();
            for(int card = 0; card < n; card++){
                PlayingCard randomCard = deck.get(random.nextInt(deck.size()));
                deal.add(randomCard);
                deck.remove(randomCard);
            }
            return deal;
        } else {
            throw new ArrayIndexOutOfBoundsException("Number must be between 1 and 52");
        }
    }

    public ArrayList<PlayingCard> getDeck(){
        return deck;
    }
}
