package edu.ntnu.idatt.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents the player's hand in the form of a list with playing cards.
 * There are no duplicate cards.
 */

public class Hand {
    private ArrayList<PlayingCard> hand;
    private final int CARDS = 10;

    /**
     * Creates an instance of a new hand with a brand new deck of cards.
     * The CARD variable determines how many playing cards the hand will contain.
     */
    public Hand(){
        DeckofCards deck = new DeckofCards();
        hand = deck.dealHand(CARDS);
    }

    public ArrayList<PlayingCard> getHand(){
      return hand;
    }

    /**
     * Returns the sum of all card faces in the hand
     *
     * @return the sum of hand
     */
    public int getSum(){
        return hand.stream().map(PlayingCard::getFace).reduce(Integer::sum).get();
    }

    /**
     * Returns all cards with hearts as suit in a string. Returns "No hearts" if the hand
     * does not contain any hearts.
     *
     * @return all hearts in the hand
     */
    public String allHearts(){
        StringBuilder result = new StringBuilder();
        if(hand.stream().anyMatch(p -> p.getSuit() == 'H')){
            List<PlayingCard> hearts = hand.stream().filter(p -> p.getSuit() == 'H').collect(Collectors.toList());
            for(PlayingCard card : hearts){
                result.append(card.getAsString()).append(" ");
            }
        } else {
            result = new StringBuilder("No hearts");
        }
        return result.toString();
    }

    /**
     * Returns true if the hand contains queen of spades, false otherwise.
     *
     * @return true if player has queen of spades
     */
    public boolean queenOfSpades(){
        return(hand.stream().anyMatch(p -> p.getAsString().equals("S12")));
    }

    /**
     * Creates a list for each suit and returns true if player has exactly 5 cards of a suit - a 5-flush.
     * This method looks for 5-flush only and will not return true if player has more than 5 cards of a suit.
     *
     * @return true if player has a 5-flush
     */
    public boolean getFlush() {
        boolean flush = false;
        int[] suitNumber = new int[4];
        suitNumber[0] = ((int) hand.stream().filter(p -> p.getSuit() == 'H').count());
        suitNumber[1] = ((int) hand.stream().filter(p -> p.getSuit() == 'S').count());
        suitNumber[2] = ((int) hand.stream().filter(p -> p.getSuit() == 'C').count());
        suitNumber[3] = ((int) hand.stream().filter(p -> p.getSuit() == 'D').count());

        for (int i : suitNumber) {
            if (i == 5) {
                flush = true;
                break;
            }
        }

        return flush;
    }
}