package edu.ntnu.idatt.cardgame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class CardgameTest {

    @Nested
    class DealHand{
        DeckofCards deck = new DeckofCards();

        @Test
        void dealTooManyCards(){
            Assertions.assertThrows(ArrayIndexOutOfBoundsException.class, () ->
                    deck.dealHand(53));
        }
        @Test
        void dealCorrectly(){
            Assertions.assertEquals(10, deck.dealHand(10).size());
            Assertions.assertEquals(42, deck.getDeck().size());
        }
    }
}