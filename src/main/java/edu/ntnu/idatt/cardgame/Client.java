package edu.ntnu.idatt.cardgame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.net.URL;

public class Client extends Application{
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = getClass().getClassLoader().getResource("cardgame.fxml");
        Pane root = FXMLLoader.load(url);
        Scene scene = new Scene(root);

        primaryStage.setTitle("Card game");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void init() throws Exception {
        super.init();
    }

    public void stop() throws Exception {
        super.stop();
    }
}
