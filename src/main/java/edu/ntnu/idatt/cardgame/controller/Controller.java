package edu.ntnu.idatt.cardgame.controller;

import edu.ntnu.idatt.cardgame.Hand;
import edu.ntnu.idatt.cardgame.PlayingCard;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class Controller {
    @FXML
    public Button dealButton;
    @FXML
    public Button checkButton;
    @FXML
    public VBox hearts;
    @FXML
    public VBox diamonds;
    @FXML
    public VBox clubs;
    @FXML
    public VBox spades;
    @FXML
    public TextField qofSBox;
    @FXML
    public TextField flushBox;
    @FXML
    public TextField heartsBox;
    @FXML
    public TextField sumBox;

    private Hand hand;

    /**
     * Initializes the controller. When Deal Hand button is clicked, a new hand will be dealt
     * with a new deck of cards and the screen will show the new cards in its own column
     * to save space in case of a big hand.
     *
     * When Check Hand button is clicked, the program will check the players hand for flush,
     * queen of spades, sum and cards of hearts.
     */
    @FXML
    public void initialize(){
        dealButton.setOnAction((event) -> {
            hand = new Hand();
            hearts.getChildren().clear();
            diamonds.getChildren().clear();
            clubs.getChildren().clear();
            spades.getChildren().clear();
                    for (PlayingCard card: hand.getHand()) {
                        if(card.getSuit() == 'H'){
                            hearts.getChildren().add(new Text(card.getAsString()));
                        } else if(card.getSuit() == 'S'){
                            spades.getChildren().add(new Text(card.getAsString()));
                        } else if(card.getSuit() == 'D'){
                            diamonds.getChildren().add(new Text(card.getAsString()));
                        } else {
                            clubs.getChildren().add(new Text(card.getAsString()));
                        }
                    }

                }
        );

        checkButton.setOnAction((event) -> {
            sumBox.setText(String.valueOf(hand.getSum()));
            if(hand.queenOfSpades()){
                qofSBox.setText("Yes");
            } else {
                qofSBox.setText("No");
            }
            heartsBox.setText(hand.allHearts());
            if(hand.getFlush()){
                flushBox.setText("Yes");
            } else {
                flushBox.setText("No");
            }
        });
    }
}
