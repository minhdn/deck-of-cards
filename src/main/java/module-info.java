module DeckofCards {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.base;

    opens edu.ntnu.idatt.cardgame to javafx.fxml;
    exports edu.ntnu.idatt.cardgame;
    exports edu.ntnu.idatt.cardgame.controller;
}